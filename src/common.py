import re

import numpy as np
import tensorflow as tf

from src.constants import IMAGE_SIZE


def get_items_count(path):

    return np.sum(
        [int(re.compile(r"-([0-9]*)\.").search(f).group(1)) for f in path]
    )


def process_image(data):
    image = tf.image.decode_jpeg(data, channels=3)  # Декодирование изображения в формате JPEG в тензор uint8.
    image = tf.cast(image, tf.float32) / 255.0  # преобразовать изображение в плавающее в диапазоне [0, 1]
    image = tf.reshape(image, [*IMAGE_SIZE, 3])  # явный размер, необходимый для TPU

    return image


def read_labeled_tf_record(example):
    example = tf.io.parse_single_example(example, {
        "image": tf.io.FixedLenFeature([], tf.string),  # tf.string означает байтовую строку
        "class": tf.io.FixedLenFeature([], tf.int64),  # [] означает отдельный элемент
    })  # парсим отдельный пример в указанном формате
    image = process_image(example['image'])  # преобразуем изображение к нужному нам формату
    label = tf.cast(example['class'], tf.int32)

    return image, label  # возвращает набор данных пар (изображение, метка)


def read_unlabeled_tf_record(example):
    example = tf.io.parse_single_example(example, {
        "image": tf.io.FixedLenFeature([], tf.string),  # tf.string означает байтовую строку
        "id": tf.io.FixedLenFeature([], tf.string),  # [] означает отдельный элемент
        # класс отсутствует, задача этого конкурса - предсказать классы цветов для тестового набора данных
    })
    image = process_image(example['image'])  # преобразуем изображение к нужному нам формату
    id_ = example['id']

    return image, id_  # returns a dataset of image(s)


def load_dataset(filenames, labeled=True, ordered=False):
    """Читает из TFRecords. Для оптимальной производительности одновременное чтение из нескольких
    файлов без учета порядка данных. Порядок не имеет значения, поскольку мы все равно будем перетасовывать данные"""

    ignore_order = tf.data.Options()  # Представляет параметры для tf.data.Dataset.
    if not ordered:
        ignore_order.experimental_deterministic = False  # отключить порядок, увеличить скорость

    dataset = tf.data.TFRecordDataset(filenames)  # автоматически чередует чтение из нескольких файлов
    dataset = dataset.with_options(ignore_order)  # использует данные сразу после их поступления, а не в исходном
    # порядке
    dataset = dataset.map(read_labeled_tf_record if labeled else read_unlabeled_tf_record)

    # возвращает набор данных пар (изображение, метка), если метка = Истина, или пар (изображение, идентификатор),
    # если метка = Ложь
    return dataset


def get_training_dataset(path, batch_size):
    dataset = load_dataset(
        tf.io.gfile.glob(path + '/tfrecords-jpeg-192x192/train/*.tfrec'),
        labeled=True
    )
    dataset = dataset.repeat()  # набор обучающих данных должен повторяться в течение нескольких эпох
    dataset = dataset.shuffle(2048)
    dataset = dataset.batch(batch_size)

    return dataset


def get_validation_dataset(path, batch_size):
    dataset = load_dataset(
        tf.io.gfile.glob(path + '/tfrecords-jpeg-192x192/val/*.tfrec'),
        labeled=True,
        ordered=False
    )
    dataset = dataset.batch(batch_size)
    dataset = dataset.cache()  # кешируем набор

    return dataset


def get_test_dataset(path, batch_size, ordered=False):
    dataset = load_dataset(
        tf.io.gfile.glob(path + '/tfrecords-jpeg-192x192/test/*.tfrec'),
        labeled=False,
        ordered=ordered
    )
    dataset = dataset.batch(batch_size)

    return dataset
