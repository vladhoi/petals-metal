from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import BatchNormalization, GaussianDropout
from tensorflow.keras.layers import Dense, Flatten

from src.constants import IMAGE_SIZE
# from tensorflow.python.keras.applications import efficientnet


def get_model():
    # feature_extractor = efficientnet.EfficientNetB7(
    #     include_top=False, input_shape=[*IMAGE_SIZE, 3])
    # feature_extractor.trainable = True

    # modeL = Sequential([
    #    feature_extractor,
    #    GlobalAveragePooling2D(),
    #    Dense(104, activation='softmax', dtype='float32')
    # ])

    model = Sequential([
        Conv2D(64, (5, 5), input_shape=(*IMAGE_SIZE, 3), activation='relu'),
        BatchNormalization(),
        Conv2D(64, (5, 5), activation='relu'),
        BatchNormalization(),
        MaxPooling2D(pool_size=(2, 2)),
        GaussianDropout(0.25),
        Conv2D(128, (5, 5), activation='relu'),
        BatchNormalization(),
        Conv2D(128, (5, 5), activation='relu'),
        MaxPooling2D(pool_size=(2, 2)),
        GaussianDropout(0.35),
        Conv2D(256, (5, 5), activation='relu'),
        BatchNormalization(),
        Conv2D(256, (5, 5), activation='relu'),
        BatchNormalization(),
        Conv2D(256, (5, 5), activation='relu'),
        BatchNormalization(),
        MaxPooling2D(pool_size=(2, 2)),
        GaussianDropout(0.45),
        Conv2D(512, (5, 5), activation='relu'),
        BatchNormalization(),
        Conv2D(512, (5, 5), activation='relu'),
        BatchNormalization(),
        Conv2D(512, (5, 5), activation='relu'),
        BatchNormalization(),
        MaxPooling2D(pool_size=(2, 2)),
        GaussianDropout(0.5),
        Flatten(),
        Dense(1024, activation='relu'),
        BatchNormalization(),
        GaussianDropout(0.8),
        Dense(104, activation='softmax')
    ])

    return model
