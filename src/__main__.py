import os

import tensorflow as tf
import numpy as np

from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau
from tensorflow.random import set_seed

from src.common import *
from src.models import get_model
from src.logger import get_module_logger

SEED = 42

# Resolve dataset
DATA_PATH = "../data/tpu-getting-started"
PICKED_DATA_PATH = DATA_PATH + '/tfrecords-jpeg-192x192'

TRAINING_DS = tf.io.gfile.glob(PICKED_DATA_PATH + '/train/*.tfrec')
VALIDATION_DS = tf.io.gfile.glob(PICKED_DATA_PATH + '/val/*.tfrec')
TEST_DS = tf.io.gfile.glob(PICKED_DATA_PATH + '/test/*.tfrec')

NUM_TRAINING_IMAGES = get_items_count(TRAINING_DS)
NUM_VALIDATION_IMAGES = get_items_count(VALIDATION_DS)
NUM_TEST_IMAGES = get_items_count(TEST_DS)

IMAGE_SIZE = [192, 192]
EPOCHS = 20

logger = get_module_logger(__name__)


def setup_seed():
    np.random.seed(SEED)
    set_seed(SEED)

    os.environ['PYTHONHASHSEED'] = str(SEED)
    os.environ['TF_DETERMINISTIC_OPS'] = '1'


def main():
    strategy = tf.distribute.get_strategy()

    BATCH_SIZE = 16 * strategy.num_replicas_in_sync
    STEPS_PER_EPOCH = NUM_TRAINING_IMAGES // BATCH_SIZE

    with strategy.scope():
        model = get_model()

    assert model.layers

    callbacks = [
        EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True),
        ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=3),
    ]

    model.compile(
        optimizer='adam',
        loss='sparse_categorical_crossentropy',
        metrics=['sparse_categorical_accuracy']
    )

    logger.info(model.summary())

    training_ds = get_training_dataset(DATA_PATH, BATCH_SIZE)
    validation_ds = get_validation_dataset(DATA_PATH, BATCH_SIZE)

    history = model.fit(
        training_ds,
        steps_per_epoch=STEPS_PER_EPOCH,
        epochs=EPOCHS,
        callbacks=callbacks,
        validation_data=validation_ds
    )

    ds = validation_ds.unbatch().batch(20)
    batch = iter(ds)

    images, labels = next(batch)
    probabilities = model.predict(images)
    predictions = np.argmax(probabilities, axis=-1)
    logger.debug(predictions)

    test_ds = get_test_dataset(ordered=True)
    logger.info('Computing predictions')
    test_images_ds = test_ds.map(lambda image, idnum: image)
    probabilities = model.predict(test_images_ds)
    predictions = np.argmax(probabilities, axis=-1)
    logger.info(predictions)

    logger.info('Generating submission.csv file')

    # convert image ids from test set to unicode
    test_ids_ds = test_ds.map(lambda image, idnum: idnum).unbatch()
    test_ids = next(iter(test_ids_ds.batch(NUM_TEST_IMAGES))).numpy().astype('U')

    np.savetxt(
        'submission.csv',
        np.rec.fromarrays([test_ids, predictions]),
        fmt=['%s', '%d'],
        delimiter=',',
        header='id,label',
        comments=''
    )


if __name__ == '__main__':
    logger.info("Tensorflow version " + tf.__version__)

    setup_seed()
    main()
