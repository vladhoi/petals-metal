import logging
from logging import Logger


def get_module_logger(module_name: str) -> Logger:
    """
    Returns logger for the provided module
    :param module_name: module name (ex: __name__)
    :return: logger instance
    """
    logger = logging.getLogger(module_name)

    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s'
    )

    handler.setFormatter(fmt=formatter)

    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    return logger
